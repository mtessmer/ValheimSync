from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5 import QtWidgets, QtCore
from pydrive.drive import GoogleDrive
from pydrive.auth import GoogleAuth, RefreshError
from pathlib import Path
from time import mktime
import sys, datetime, os, pytz

# Get current timezone and dst information
timezone = datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo

def get_drive(client_secrets, cred_file):
    GoogleAuth.DEFAULT_SETTINGS['client_config_file'] = client_secrets
    gauth = GoogleAuth()
    gauth.LoadCredentialsFile(cred_file)

    if gauth.credentials is None:
        gauth.LocalWebserverAuth()
    elif gauth.access_token_expired:
        try:
            gauth.Refresh()
        except RefreshError:
            with open(cred_file, 'w') as f:
                f.write('')
            gauth.LoadCredentialsFile(cred_file)
            gauth.LocalWebserverAuth()
    else:
        gauth.Authorize()
    gauth.SaveCredentialsFile(cred_file)

    return GoogleDrive(gauth)

def get_folderid(file):
    try:
        with open(file, 'r') as f:
            file_id = f.readline().strip()
        return file_id
    except FileNotFoundError:
        return None

def set_folderid(file, folder_id):
    with open(file, 'w') as f:
        f.write(folder_id)

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.pad = 25
        self.list_height, self.list_width = 400, 400
        self.window_height, self.window_width = self.list_height + 100, self.list_width + 50
        self.setWindowTitle("ValheimSync")
        self.resize(self.window_width, self.window_height)

        self.centralwidget = QtWidgets.QWidget(self)
        self.worldListWidget = WorldList(self.centralwidget)
        self.worldListWidget.setGeometry(QtCore.QRect(self.pad, self.pad, self.list_width, self.list_height))

        self.buttonBoxWidget = QtWidgets.QWidget(self.centralwidget)
        self.buttonBoxLayout = QtWidgets.QHBoxLayout(self.buttonBoxWidget)
        self.buttonBoxWidget.setGeometry(QtCore.QRect(self.pad, self.pad + self.list_height, self.list_width, 50))

        self.Sync = QtWidgets.QPushButton('Sync', self.buttonBoxWidget)
        self.Sync.clicked.connect(self._sync)

        self.buttonBoxLayout.addWidget(self.Sync)

        self.drive = get_drive(auth_file, cred_file)
        self._check()

        self.setCentralWidget(self.centralwidget)

    def _check(self):
        self.drive_file_list = self.drive.ListFile(
            {'q': f"'{folder_id}' in parents and trashed=false"}).GetList()

        for file in self.drive_file_list:
            if file['title'][-2:] == 'db':
                self.worldListWidget.add_from_drive(file['title'][:-3])

        self.filedict = {}
        for idx, item in enumerate(self.list_items):
            self.filedict[item.text()] = {}
            for file in self.drive_file_list:
                comp = file['title'].rsplit(".", 1)
                if item.text() == comp[0]:
                    self.filedict[item.text()][file['title'].split('.')[-1]] = file

                if item.text() == comp[0] and comp[1] == 'db':
                    self.filedict[item.text()]['date'] = datetime.datetime.strptime(file['modifiedDate'],
                                                                                    "%Y-%m-%dT%H:%M:%S.%fZ")
                    self.filedict[item.text()]['date'] = \
                        self.filedict[item.text()]['date'].replace(tzinfo=datetime.timezone.utc,
                                                                   second=0,
                                                                   microsecond=0).astimezone(tz=None)

            if 'date' not in self.filedict[item.text()]:
                pass
            elif self.worldListWidget.modTimes[idx] == 0:
                item.setForeground(QtCore.Qt.gray)
            elif self.worldListWidget.modTimes[idx] == self.filedict[item.text()]['date']:
                item.setForeground(QtCore.Qt.green)
            elif self.worldListWidget.modTimes[idx] < self.filedict[item.text()]['date']:
                print(item.text(), self.worldListWidget.modTimes[idx], self.filedict[item.text()]['date'])
                item.setForeground(QtCore.Qt.red)
            elif self.worldListWidget.modTimes[idx] > self.filedict[item.text()]['date']:
                print(item.text(), self.worldListWidget.modTimes[idx], self.filedict[item.text()]['date'])
                item.setForeground(QtCore.Qt.blue)

    def _sync(self):
        # Get index of current item
        for idx, item in enumerate(self.list_items):
            if item == self.worldListWidget.currentItem():
                break

        if 'date' not in self.filedict[item.text()]:
            print('Local is newer than remote. Uploading...')
            for key in ['db', 'fwl']:
                fpath = str(self.worldListWidget.ValheimDir / (item.text() + '.' + key))
                f = self.drive.CreateFile({'parents': [{'id': folder_id}],
                                           'title': (item.text() + '.' + key)})
                f.SetContentFile(fpath)
                f['modifiedDate'] = self.worldListWidget.modTimes[idx].astimezone(pytz.utc).strftime("%Y-%m-%dT%H:%M:%S.%fZ")
                f.Upload()

        elif self.worldListWidget.modTimes[idx] == 0:
            print('Local is older than remote. Downloading...')
            for key in self.filedict[item.text()]:
                if key == 'date':
                    continue
                fpath = str(self.worldListWidget.ValheimDir / (item.text() + '.' + key))
                self.filedict[item.text()][key].GetContentFile(fpath)
                modTime = mktime(self.filedict[item.text()]['date'].timetuple())
                os.utime(fpath, (modTime, modTime))

            time = datetime.datetime.strptime(self.filedict[item.text()]['db']['modifiedDate'], "%Y-%m-%dT%H:%M:%S.%fZ")
            time = time.replace(tzinfo=datetime.timezone.utc,
                                second=0,
                                microsecond=0).astimezone(tz=None)

            self.worldListWidget.modTimes[idx] = time


        elif self.worldListWidget.modTimes[idx] > self.filedict[item.text()]['date']:
            print('Local is newer than remote. Uploading...')
            for key in self.filedict[item.text()]:
                if key == 'date':
                    continue

                fpath = str(self.worldListWidget.ValheimDir / (item.text() + '.' + key))
                f = self.filedict[item.text()][key]
                f.SetContentFile(fpath)
                f['modifiedDate'] = self.worldListWidget.modTimes[idx].astimezone(pytz.utc).strftime(
                    "%Y-%m-%dT%H:%M:%S.%fZ")
                print(f['modifiedDate'])
                params = {'setModifiedDate': 'fromBody'}
                f.Upload(param=params)
                f.FetchMetadata()



        elif self.worldListWidget.modTimes[idx] < self.filedict[item.text()]['date']:
            print('Local is older than remote. Downloading...')
            for key in self.filedict[item.text()]:
                if key == 'date':
                    continue
                fpath = str(self.worldListWidget.ValheimDir / (item.text() + '.' + key))
                self.filedict[item.text()][key].GetContentFile(fpath)
                modTime = mktime(self.filedict[item.text()]['date'].timetuple())
                os.utime(fpath, (modTime, modTime))

            time = datetime.datetime.strptime(self.filedict[item.text()]['db']['modifiedDate'], "%Y-%m-%dT%H:%M:%S.%fZ")
            time = time.replace(tzinfo=datetime.timezone.utc,
                                second=0,
                                microsecond=0).astimezone(tz=None)

            self.worldListWidget.modTimes[idx] = time

        self.worldListWidget.reinit()
        self._check()

    @property
    def list_items(self):
        return [self.worldListWidget.item(i) for i in range(self.worldListWidget.count())]


class WorldList(QtWidgets.QListWidget):

    def __init__(self, parent=None):

        super(WorldList, self).__init__(parent)

        self. ValheimDir = Path.home() / 'AppData/LocalLow/IronGate/Valheim/worlds'

        # Get local world files
        self.worlds = list(self.ValheimDir.glob('*.db'))

        # Get universal times
        self.modTimes = [datetime.datetime.fromtimestamp(x.stat().st_mtime) for x in self.worlds]
        self.modTimes = [x.replace(tzinfo=timezone, second=0, microsecond=0).astimezone() for x in self.modTimes]

        # Generate list
        self.addItems([p.stem for p in self.worlds])


    def reinit(self):
        self.clear()

        self.worlds = list(self.ValheimDir.glob('*.db'))

        # Get universal times
        self.modTimes = [datetime.datetime.fromtimestamp(x.stat().st_mtime) for x in self.worlds]
        self.modTimes = [x.replace(tzinfo=timezone, second=0, microsecond=0).astimezone() for x in self.modTimes]

        # Generate list
        self.addItems([p.stem for p in self.worlds])

    def add_from_drive(self, item):
        if item not in self.item_set:
            self.addItem(item)
            self.modTimes.append(0)

    @property
    def item_set(self):
        return {self.item(i).text() for i in range(self.count())}

def prompt_user_link():
    msg = QtWidgets.QMessageBox()
    text, ok = QtWidgets.QInputDialog.getText(msg, 'Set Server Link', 'Server Link:', QtWidgets.QLineEdit.Normal, "")
    text = text.strip().split('/')[-1]
    text = text.split('?')[0]
    return text

if __name__ == '__main__':
    appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext
    auth_file = appctxt.get_resource('client_secret.json')
    linkfile = appctxt.get_resource('link.txt')
    folder_id = get_folderid(linkfile)
    cred_file = appctxt.get_resource('cred_file.txt')

    if folder_id == '':
        folder_id = prompt_user_link()
        set_folderid(linkfile, folder_id)

    window = MainWindow()
    window.show()

    exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
    sys.exit(exit_code)